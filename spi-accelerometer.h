#ifndef SPI_ACCELEROMETER_H
#define SPI_ACCELEROMETER_H
#include <stdint.h>

typedef struct
{	
	int16_t x;
	int16_t y;
	int16_t z;
} vec;

typedef struct
{
	uint8_t id; 
	vec acc;	
	vec mag;
	vec grav;
	int16_t t;
} magAndAccValue; //ZMIENIĆ NAZWĘ NA "DATA"!!!!!!!!!!!!!!!!!!!!!!!!!!!

int8_t transceiveData(int8_t data);
void SPI_MasterTranseive( uint8_t *dataIn, const int8_t *dataOut, unsigned dataLength);
void writeRegister(int8_t address, int8_t value);
void accelerometerInit(void);
void readData(uint8_t *dataIn, int8_t address);
void concatenateData(magAndAccValue *value);
void multiReadData(uint8_t* dataIn, int8_t address);

#endif /*SPI-ACCELEROMETER_H*/
