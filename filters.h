#ifndef FILTERS_H
#define FILTERS_H
#include <stdint.h>

void lowPassFilter(magAndAccValue *val, magAndAccValue *previousVal);

#endif /*FILTERS_H */
