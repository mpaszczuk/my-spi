import numpy as np
import pygame
from OpenGL.GL import *
from OpenGL.GLU import *
from pygame.constants import DOUBLEBUF, OPENGL
from cube import *
from serialcommunicator import *
from angledetector import *


class SpiCube:
    def __init__(self):
        self.data_processing = DataProcessing("/dev/ttyUSB0")
        self.display = (800, 600)  # surface/canvas we will draw things to 800px wide 600 px tall
        self.acc_filter = Filter()
        self.mag_filter = Filter()
        self.angle_detector = AngleDetector()
        self.cube = Cube()

    def filters_init(self):
        while not all(
                (
                        self.acc_filter.is_matrix_filled,
                        self.mag_filter.is_matrix_filled,
                )
        ):
            self.data_processing.read_data()
            if self.data_processing.data_is_valid:
                acceleration, magnetic_field = self.data_processing.data
                self.acc_filter.add_new_data(acceleration)
                self.mag_filter.add_new_data(magnetic_field)

        while not all(
                (
                        self.acc_filter.is_median_matrix_filled,
                        self.mag_filter.is_median_matrix_filled,
                )
        ):
            acceleration, magnetic_filed = self.data_processing.data
            self.acc_filter.add_new_data(acceleration)
            self.mag_filter.add_new_data(magnetic_field)
            self.acc_filter.get_data()
            self.mag_filter.get_data()

    def display_init(self):
        pygame.init()
        pygame.display.set_mode(self.display,
                                DOUBLEBUF | OPENGL)  # telling we're gonna use opengl and doublebuffer (to comply with monitro refresh rate
        gluPerspective(45, (self.display[0] / self.display[1]), 0.1,
                       50.0)  # 45-degree, second-acpect ration width/height, last to znear zfar clipping planes (tak jakby perspektywa)
        glTranslatef(0.0, 0.0, -5)  # we're moving back 5 unites so we can see th cube

    def init(self):
        self.data_processing.connect()
        self.filters_init()
        self.angle_detector.create_rotation_matrix_v1(self.acc_filter.get_data(),
                                                      self.mag_filter.get_data())
        self.display_init()

    def display_loop(self):
        while True:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    pygame.quit()
                    quit()

            self.data_processing.read_data()
            if self.data_processing.data_is_valid:
                self.acc_filter.add_new_data(self.data_processing.acc)
                self.mag_filter.add_new_data(self.data_processing.mag)
                self.angle_detector.create_rotation_matrix_v1(self.acc_filter.get_data(),
                                                              self.mag_filter.get_data())

            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
            self.cube.calculate_rotated_vertices(self.angle_detector.rotation_matrix_v1)

            self.cube.draw()
            pygame.display.flip()  # updates display
            pygame.time.wait(1)


if __name__ == "__main__":
    spi_cube = SpiCube()
    spi_cube.init()
    spi_cube.display_loop()
