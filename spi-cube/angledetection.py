import numpy as np


class Filter:
    window_length = 5

    def __init__(self):
        self.median_matrix = np.array([[0, 0, 0]])
        self.average_matrix = []
        self.matrix = []

    def sliding_median_filter(self):
        norm_matrix = []

        for item in self.matrix:
            norm = np.linalg.norm(item, ord=1)
            norm_matrix.append(norm)

        median_value = np.median(norm_matrix)
        where_out = np.where(norm_matrix == median_value)
        index = where_out[0][0]
        filtered_value = self.matrix[index]
        self.median_matrix = np.append(self.median_matrix, [filtered_value], axis=0)
        if len(self.median_matrix) > Filter.window_length:
            self.median_matrix = self.median_matrix[1:]

    def sliding_average_filter(self):
        sum_matrix = sum(self.median_matrix)
        self.average_matrix = sum_matrix / len(self.median_matrix)

    def add_new_data(self, new_data):
        self.matrix.append(new_data)
        if len(self.matrix) > Filter.window_length:
            self.matrix.pop(0)

    def get_data(self):
        self.sliding_median_filter()
        self.sliding_average_filter()
        return self.average_matrix

    @property
    def is_matrix_filled(self):
        if len(self.matrix) == Filter.window_length:
            return True
        else:
            return False

    @property
    def is_median_matrix_filled(self):
        if len(self.median_matrix) == Filter.window_length:
            return True
        else:
            return False


class AngleDetector:
    delta = 1.15

    def __init__(self):  # kalibracja
        self.roll = 0
        self.pitch = 0
        self.yaw = 0
        self.prev_roll = 0
        self.prev_pitch = 0
        self.prev_yaw = 0
        self.roll_rotation = None
        self.pitch_rotation = None
        self.yaw_rotation = None
        self.singularity_detected = False
        self.filtered_acceleration = None
        self.filtered_magnetic_field = None
        self._rotation_matrix_v1 = None

    def compute_roll_angle(self):
        self.roll = np.arctan2(self.filtered_acceleration[1], self.filtered_acceleration[2])

    def compute_pitch_angle(self):
        nominator = -self.filtered_acceleration[0]
        denominator = self.filtered_acceleration[1] * np.sin(self.roll) + self.filtered_acceleration[2] * np.cos(
            self.roll)
        angle = nominator / denominator
        self.pitch = np.arctan(angle)

    def compute_yaw_angle(self):
        nominator = self.filtered_magnetic_field[2] * 0.0001 * np.sin(self.roll) - self.filtered_magnetic_field[
            1] * 0.0001 * np.cos(self.roll)
        denominator = self.filtered_magnetic_field[0] * 0.0001 * np.cos(self.pitch) + self.filtered_magnetic_field[
            1] * 0.0001 * np.sin(
            self.roll) * np.sin(self.pitch) + \
                      self.filtered_magnetic_field[2] * 0.0001 * np.sin(self.pitch) * np.cos(self.roll)
        self.yaw = np.arctan2(nominator, denominator)

    def create_roll_rotation(self):
        self.roll_rotation = np.array([[1, 0, 0],
                                       [0, np.cos(self.roll), np.sin(self.roll)],
                                       [0, -np.sin(self.roll), np.cos(self.roll)]])

    def create_pitch_rotation(self):
        self.pitch_rotation = np.array([[np.cos(self.pitch), 0, -np.sin(self.pitch)],
                                        [0, 1, 0],
                                        [np.sin(self.pitch), 0, np.cos(self.pitch)]])

    def create_yaw_rotation(self):
        self.yaw_rotation = np.array([[np.cos(self.yaw), np.sin(self.yaw), 0],
                                      [-np.sin(self.yaw), np.cos(self.yaw), 0],
                                      [0, 0, 1]])

    def create_rotation_matrix(self):
        rotation_matrix = self.roll_rotation * self.pitch_rotation * self.yaw_rotation
        rotation_matrix = np.append(rotation_matrix, [[0, 0, 0]], axis=0)
        rotation_matrix = np.append(rotation_matrix, [[0], [0], [0], [1]], axis=1)
        rotation_matrix = rotation_matrix.flatten('F')
        return rotation_matrix

    def detect_singularity(self):
        norm_acc_value = np.linalg.norm(self.filtered_acceleration, ord=1)
        normalized_grav_field = self.filtered_acceleration / norm_acc_value

        norm_mag_value = np.linalg.norm(self.filtered_magnetic_field)
        normalized_mag_field = self.filtered_magnetic_field / norm_mag_value

        if any(
                np.allclose(
                    [
                        normalized_grav_field[0],
                        normalized_grav_field[1],
                        normalized_mag_field[0],
                        normalized_mag_field[1],
                    ],
                    vector,
                    atol=0.1,
                    rtol=0,
                )
                for vector in (
                        [1, 0, np.sin(self.delta), np.cos(self.delta)],
                        [1, 0, np.sin(self.delta), -np.cos(self.delta)],
                        [0, -1, np.cos(self.delta), -np.sin(self.delta)],
                        [0, -1, -np.cos(self.delta), -np.sin(self.delta)],
                        [-1, 0, -np.sin(self.delta), -np.cos(self.delta)],
                        [-1, 0, -np.sin(self.delta), np.cos(self.delta)],
                        [0, -1, -np.cos(self.delta), np.sin(self.delta)],
                        [0, -1, np.cos(self.delta), np.sin(self.delta)],
                )
        ):
            self.singularity_detected = True
        else:
            self.singularity_detected = False

    def store_prev_angles(self):

        self.prev_roll = self.roll
        self.prev_pitch = self.pitch
        self.prev_yaw = self.yaw

    def create_rotation_matrix_v1(self, acceleration, magnetic_field):

        cross_product = np.cross(acceleration, magnetic_field)
        normalized_cross_product = np.linalg.norm(cross_product)
        c = cross_product / normalized_cross_product

        normalized_magnetic_field = np.linalg.norm(magnetic_field, ord=1)
        b = magnetic_field / normalized_magnetic_field

        t = np.cross(b, c)

        self._rotation_matrix_v1 = np.array([[b[0] * np.cos(AngleDetector.delta) - t[0] * np.sin(AngleDetector.delta),
                                             c[0],
                                             b[0] * np.sin(AngleDetector.delta) + t[0] * np.cos(AngleDetector.delta)],
                                            [b[1] * np.cos(AngleDetector.delta) - t[1] * np.sin(AngleDetector.delta),
                                             c[1],
                                             b[1] * np.sin(AngleDetector.delta) + t[1] * np.cos(AngleDetector.delta)],
                                            [b[2] * np.cos(AngleDetector.delta) - t[2] * np.sin(AngleDetector.delta),
                                             c[2],
                                             b[2] * np.sin(AngleDetector.delta) + t[2] * np.cos(AngleDetector.delta)]])

    def specify_angles(self, acceleration, magnetic_field):

        self.filtered_acceleration = acceleration
        self.filtered_magnetic_field = magnetic_field
        self.store_prev_angles()
        self.detect_singularity()

        if self.singularity_detected:
            return
        else:
            self.compute_roll_angle()
            self.compute_pitch_angle()
            self.compute_yaw_angle()

    @property
    def rotation_angles(self):
        return self.roll, self.pitch, self.yaw

    @property
    def rotation_angles_deg(self):
        return (
            self.roll * 180 / np.pi,
            self.pitch * 180 / np.pi,
            self.yaw * 180 / np.pi,
        )

    @property
    def previous_rotation_angles(self) -> (float, float, float):
        return self.prev_roll, self.prev_pitch, self.prev_yaw

    @property
    def previous_rotation_angles_deg(self) -> (float, float, float):
        return (
            self.prev_roll * 180 / np.pi,
            self.prev_pitch * 180 / np.pi,
            self.prev_yaw * 180 / np.pi,
        )

    @property
    def rotation_diff(self):
        return (
            self.roll - self.prev_roll,
            self.pitch - self.prev_pitch,
            self.yaw - self.prev_yaw,
        )

    @property
    def rotation_diff_deg(self):
        return (
            (self.roll - self.prev_roll) * 180 / np.pi,
            (self.pitch - self.prev_pitch) * 180 / np.pi,
            (self.yaw - self.prev_yaw) * 180 / np.pi,
        )

    @property
    def rotation_matrix_v1(self):
        return self._rotation_matrix_v1
