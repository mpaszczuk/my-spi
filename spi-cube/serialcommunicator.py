import serial


class DataValidationHasNotBeenCalled(Exception):
    pass


class DataIsInvalid(Exception):
    pass


class DataProcessing:
    FILTER_LEN = 5

    def __init__(self, port_name):
        self.port_name = port_name
        self.ser = None
        self.unprocessed_data = None
        self._data_is_valid = True
        self.data_has_been_validated = False
        self._data = None
        self._acc = None
        self._mag = None

    def connect(self):
        self.ser = serial.Serial(self.port_name, timeout=1, parity=serial.PARITY_ODD)

    def read_data(self):
        self.unprocessed_data = self.ser.readline()
        # value = value.split()

    def parse_data(self):
        try:
            decoded_data = self.unprocessed_data.decode("utf-8")
        except UnicodeDecodeError:
            self._data_is_valid = False
            return
        self._data = decoded_data.split()
        if self._data[0] == "73":
            self._data_is_valid = True
        else:
            self._data_is_valid = False
        try:
            self._acc = [int(x) for x in self._data[1:4]]
            self._mag = [int(x) for x in self._data[4:7]]
        except ValueError:
            self._data_is_valid = False
        self.data_has_been_validated = True

    @property
    def data_is_valid(self):
        self.parse_data()
        return self._data_is_valid

    @property
    def data(self):
        if not self.data_has_been_validated:
            raise DataValidationHasNotBeenCalled()
        if self._data_is_valid:
            return self._acc, self._mag
        else:
            raise DataIsInvalid()

    @property
    def acc(self):
        if not self.data_has_been_validated:
            raise DataValidationHasNotBeenCalled()
        if self._data_is_valid:
            return self._acc
        else:
            raise DataIsInvalid()

    @property
    def mag(self):
        if not self.data_has_been_validated:
            raise DataValidationHasNotBeenCalled()
        if self._data_is_valid:
            return self._mag
        else:
            raise DataIsInvalid()
