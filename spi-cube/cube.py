import numpy as np
from OpenGL.GL import *
from angledetector import *


class Cube:
    # location of each vertex in space, points connected would form a cube
    vertices = np.array([[1, -1, -1],
                         [1, 1, -1],
                         [-1, 1, -1],
                         [-1, -1, -1],
                         [1, -1, 1],
                         [1, 1, 1],
                         [-1, -1, 1],
                         [-1, 1, 1]])

    # each tuple contains two vertices
    edges = ((0, 1),
             (0, 3),
             (0, 4),
             (2, 1),
             (2, 3),
             (2, 7),
             (6, 3),
             (6, 4),
             (6, 7),
             (5, 1),
             (5, 4),
             (5, 7))

    surfaces = ((0, 1, 2, 3),
                (3, 2, 7, 6),
                (6, 7, 5, 4),
                (4, 5, 1, 0),
                (1, 5, 7, 2),
                (4, 0, 3, 6))

    colors = ((1, 0, 0),
              (1, 0, 1),
              (0, 0, 1),
              (0, 0, 0),
              (1, 1, 1),
              (1, 0, 1),
              (1, 0, 0),
              (1, 0, 1),
              (0, 0, 1),
              (0, 0, 0),
              (1, 1, 1),
              (1, 0, 1))

    def __init__(self):
        self.rotated_vertices = None

    def calculate_rotated_vertices(self, rotation_matrix):
        self.rotated_vertices = np.dot(self.vertices, rotation_matrix).tolist()

    def draw(self):
        glBegin(GL_QUADS)
        for surface in self.surfaces:
            x = 0
            for vertex in surface:
                x += 1
                glColor3fv(self.colors[x])
                # glVertex3fv(vertices[vertex])
                glVertex3fv(self.rotated_vertices[vertex])
        glEnd()

        glBegin(GL_LINES)  # notifes OPENGL that we're about to code smth with aim to draw lines (hence GLLINES)
        for edge in self.edges:
            for vertex in edge:
                # glVertex3fv(vertices[vertex])
                glVertex3fv(self.rotated_vertices[vertex])
        glEnd()
