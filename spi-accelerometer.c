#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "spi-accelerometer.h"

/*Defining addreses of acceleration in three directions*/

#define OUT_X_L_A   0x28 
#define OUT_X_H_A   0x29
#define OUT_Y_L_A   0x2A
#define OUT_Y_H_A   0x2B
#define OUT_Z_L_A   0x2C
#define OUT_Z_H_A   0x2D

#define OUT_X_L_M   0x08
#define OUT_X_H_M   0x09
#define OUT_Y_L_M   0xA
#define OUT_Y_H_M   0xB
#define OUT_Z_L_M   0xC
#define OUT_Z_H_M   0xD

#define TEMP_OUT_L 0x05
#define TEMP_OUT_H 0x06 

/*Defining control registers (CTRLx) which will be used/needed */

#define CTRL1       0x20
#define CTRL7       0x26 

#define CTRL5       0x24

/*Defining write/read commands from datasheet*/

#define READDATA 0x80
#define WRITEDATA 0x00
#define MULTIREAD 0x40

#define SS PB2

#define WHO_AM_I 0xF

int8_t transceiveData(int8_t data)
{ 
	/* Starting transmission */
	SPDR = data;

	/* Waiting for transmission to be completed */
	while(!(SPSR & _BV(SPIF))) ;

	return SPDR;
}


void SPI_MasterTranseive( uint8_t *dataIn, const int8_t *dataOut, unsigned dataLength)
{

	for(int i=0; i<dataLength; i++)
	{
		/*Transmitting next elements from microcontroller */
	
		SPDR=dataOut[i];

		/*Waiting for transmisson to be completed */

		while(!(SPSR & _BV(SPIF))) ;

		/*Receiving next elements from accelometer */

		dataIn[i]=SPDR;

	}
}

void writeRegister(int8_t address, int8_t value)
{
	/*One byte solution*/

	/*Setting SS low*/

	PORTB &= ~(_BV(SS)); 

	/*Firstly sending an addres in WRITE command and then sending value of the addres */ 
		
	transceiveData(address);

	transceiveData(value);

	/*Setting SS high*/

	PORTB |= (_BV(SS));
}

void multiReadData(uint8_t* dataIn, int8_t address)
{
	/*Multi-byte solution*/

	/*Setting SS low*/

	PORTB &= ~(_BV(SS)); 

	int8_t dataToSend= MULTIREAD |READDATA | address;

	const int8_t dataOut[7]={dataToSend};

	/*Sending an addres in READ command and then sending the dummy in order to retrieve
	  reciprocal bytes which carry information about designated address */	

	SPI_MasterTranseive(dataIn, dataOut, 7);

	/*Setting SS high*/

	PORTB |= (_BV(SS));
}

void readData(uint8_t* dataIn, int8_t address)
{
	/*Multi-byte solution*/

	/*Setting SS low*/

	PORTB &= ~(_BV(SS)); 

	int8_t dataToSend= READDATA | address;

	const int8_t dataOut[2]={dataToSend, 0x00};

	/*Sending an addres in READ command and then sending the dummy in order to retrieve
	  reciprocal bytes which carry information about designated address */	

	SPI_MasterTranseive(dataIn, dataOut, 2);

	/*Setting SS high*/

	PORTB |= (_BV(SS));
}

void accelerometerInit(void) //sprawdzic czy jakis nie brakuje
{
	/*Telling microcontroller which control registers of chip are used and in 
	  which configuration */

	writeRegister(CTRL1, 0x37);
  	writeRegister(CTRL5, 0xEC);  // control registers
	writeRegister(CTRL7, 0x00);
}

void concatenateData(magAndAccValue *value)
{
	
	uint8_t wai[2], ma[7], mm[7], mt[3];


	/*Retreiving the information about designated addresses from readData function */

	multiReadData(ma, OUT_X_L_A);
	multiReadData(mm, OUT_X_L_M);
	multiReadData(mt, TEMP_OUT_L);	

	readData(wai, WHO_AM_I);

	/*Concatenating two values (HIGH and LOW) of the same register into one */


	value->id=wai[1];

        value->acc.x = ma[2] << 8 | ma[1];
        value->acc.y = ma[4] << 8 | ma[3];
        value->acc.z = ma[6] << 8 | ma[4];

	value->mag.x= mm[2] << 8 | mm[1];
        value->mag.y= mm[4] << 8 | mm[3];
        value->mag.z= mm[6] << 8 | mm[4];

	value->t= mt[2] << 8 | mt[1];

}


