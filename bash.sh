#!/bin/bash

# Usage: ./flash.sh <SERIAL_PORT> <HEX_FILE>

if [ $# -eq 0 ]; then
	SERIAL_PORT="/dev/ttyUSB0"
else
	SERIAL_PORT=$1
fi

if [ $# -lt 2 ]; then
	HEX_FILE="spi-acc.hex"
else
	HEX_FILE=$2
fi


if [ ! -f "$HEX_FILE" ]; then
	echo -n "$HEX_FILE not found"
	exit 1
fi

if [ ! -e "$SERIAL_PORT" ]; then
	echo -n "$SERIAL_PORT not found"
	exit 1
fi

avrdude -patmega328p -carduino -P"$SERIAL_PORT" -b115200 -D -Uflash:w:"$HEX_FILE":i
