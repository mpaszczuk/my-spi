#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "spi-master.h"
 
#define MAX 255
#define SS PB2
#define MOSI PB3
#define	MISO PB4
#define SCK PB5

void SPI_MasterInit(void)
{
	/*Data Direction Register for SPI is B */

	/*Setting Slave Select(SS), Master out Slave In (MOSI), SCK as output pins */
	DDRB |= (_BV(MOSI)|_BV(SCK)|_BV(SS));
	
	/*Setting Master in Slave Out (MISO) as an input pin*/
	DDRB &=~ _BV(MISO);
	
	/*Enabling the SPI mode (SPE) on SPI Control Register (SPCR)*/
	SPCR |= _BV(SPE) |_BV(MSTR)| _BV(CPOL)|_BV(CPHA)|_BV(SPR1) | _BV(SPR0) ; 

	/*MSTR set to 1 in order to be in a Master Mode */

	/*Adding clock polarity (CPOL), clock phase (CPHA),
	  and SPR0 i SPR1, remember to check it again */

	PORTB |= (_BV(SS)); 


}

/*SPI is full duplex. The master controls the clock, with a data bit being shifted in both 
  directions on each pulse. So in order to receive a byte, you must transmit a byte. Depending
  on the protocol, the byte sent [or received] may or may not have any meaning.*/

