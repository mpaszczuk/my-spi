#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "spi-master.h"
#include "spi-accelerometer.h"
#include "uart.h"
#include "filters.h"


#define WHO_AM_I 0xF

int main (void)
{
	static magAndAccValue val;
	static magAndAccValue prevVal;

	uart_init(9600);
	SPI_MasterInit();
	accelerometerInit();

	concatenateData(&prevVal);

	while(1)
	{
		concatenateData(&val);
		lowPassFilter(&val, &prevVal);
		prevVal=val;

		uart_printf("%u ",val.id);
		uart_printf("%d %d %d ",val.grav.x, val.grav.y, val.grav.z);
		uart_printf("%d %d %d", val.mag.x, val.mag.y, val.mag.z);
		uart_printf("\r\n"); 
		_delay_ms(10);
	}
	return 0;
}

