//ZMIENIĆ NAZYWW!!!!!!!!!!!!!!!!!!!!!!!!

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include "spi-accelerometer.h"

void lowPassFilter(magAndAccValue *val, magAndAccValue *previousVal)
{
	/*Coefficeints calculeted in Python, using first order butterwoth filter */
	
	float b0=0.0591907;
	float b1=0.0591907;

	float a1=0.88161859; 

	/*Variables about current acceleration data and previos acceleration and
	  gravitational data */	
	
	int16_t ax, ay, az, prevAx, prevAy, prevAz, prevGx, prevGy, prevGz;

	ax=val->acc.x;
	ay=val->acc.y;
	az=val->acc.z;
	
	prevAx= previousVal->acc.x;
	prevAy= previousVal->acc.y;
	prevAz= previousVal->acc.z;
	
	prevGx= previousVal->grav.x;
	prevGy= previousVal->grav.y;
	prevGz= previousVal->grav.z;

	/*First-order Butterworth filter, after changing Z-tranform for impulse responese */
	
	val->grav.x= b0*ax + b1*prevAx + a1*prevGx;
	val->grav.y= b0*ay + b1*prevAy + a1*prevGy;
	val->grav.z= b0*az + b1*prevAz + a1*prevGz;
}


//TODO: wykrywanie osobvliwości
	



